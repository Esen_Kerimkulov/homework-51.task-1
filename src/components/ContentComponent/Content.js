import React from 'react';
import picture from '../../img/vprityk.jpg';
import picture2 from '../../img/chtivo.jpg';
import picture3 from '../../img/2stvola.jpg';

const Content = () => {
    return(
        <div className="films">
            <div className="container">
                <h1 className="title-main">My favorite films</h1>
                <div className="films__items">
                    <figure className="film-item">
                        <a href="#" className="film-item__link">
                            <img className="film-item__img" src={picture2} alt="_"/>
                        </a>
                        <figcaption className="film-item__descr">Криминальное чтиво</figcaption>
                        <p className="year">1994</p>
                    </figure>
                    <figure className="film-item">
                        <a href="#" className="film-item__link">
                            <img className="film-item__img" src={picture} alt="_"/>
                        </a>
                        <figcaption className="film-item__descr">Впритык</figcaption>
                        <p className="year">2010</p>
                    </figure>
                    <figure className="film-item">
                        <a href="#" className="film-item__link">
                            <img className="film-item__img" src={picture3} alt="_"/>
                        </a>
                        <figcaption className="film-item__descr">Два ствола</figcaption>
                        <p className="year">2013</p>
                    </figure>
                    </div>
            </div>
        </div>
    )
};
            export default Content;